# Git 使用文档

## 分支                 
列出所有本地分支
```
git branch 
```
列出远程和本地分支
```
git branch -a
```
创建一个本地分支并切换到它
```
git checkout -b branch_name
```
切换到现有分支
```
git checkout branch_name
```
将分支推到远程
```
git push origin branch_name
```
重命名当前分支
```
git branch -m new_name
```
删除本地分支
```
git branch -d branch_name
```
删除远程分支
```
git push origin :branch_name
```

## 操作日志                 
以单行形式显示提交历史记录
```
git log --oneline
```
查看提交日志
```
git log
```
显示最后N次提交的提交历史记录
```
git log -2 
```
使用差异显示最后N次提交的提交历史记录
```
git log -p -2
```
在工作树中显示所有本地文件更改
```
git diff
```
显示对文件所做的更改
```
git diff myfile
```
显示谁更改了文件中的内容和时间
```
git blame myfile 
```
显示远程分支及其到本地的映射
```
git remote show origin 
```

## 删除  
删除所有未跟踪的文件    
```
git clean -f
```
删除所有未跟踪的文件和目录
```
git clean -df
```
撤消对所有文件的本地修改
```
git checkout -- .
```
取消暂存文件
```
git reset HEAD myfile
```

## 标签
获取远程标记
```
git pull --tags    
```
切换到现有标记
```
git checkout tag_name
```
列出所有标签
```
git tag
```
创建一个新标记
```
git tag -a tag_name -m 'tag message'
```
将所有标签推送到远程仓库
```
git push --tags
```

## 存储
将更改保存到存储库中。
```
git stash save 'stash_name' && git stash
```
列出所有藏匿物品。
```
git stash list
```
应用存储并将其从存储列表中删除
```
git stash pop
```