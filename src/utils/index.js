/**
 * 生成随机id
 */
export function randomNum() {
  let vNow = new Date();
  let sNow = "";
  sNow += String(vNow.getFullYear());
  sNow += String(vNow.getMonth() + 1);
  sNow += String(vNow.getDate());
  sNow += String(vNow.getHours());
  sNow += String(vNow.getMinutes());
  sNow += String(vNow.getSeconds());
  sNow += String(vNow.getMilliseconds());
  return sNow
}