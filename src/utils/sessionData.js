const dict = {
  0: 'nav',
}
export const getKey = (key) => {
  return dict[key]
}
export function getSessionData(sessionKey) {
  const obj = sessionStorage.getItem(sessionKey)
  return obj === null ? null : JSON.parse(obj)
}
export function setSessionData(localKey, data) {
  const result = JSON.stringify(data)
  sessionStorage.setItem(localKey, result)
}
export function removeSessionData(localKey) {
  sessionStorage.removeItem(localKey)
}