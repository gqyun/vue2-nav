import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import ElementUI from 'element-ui';
import 'element-ui/lib/theme-chalk/index.css';
import VueLazyload from 'vue-lazyload'
import axios from "axios";
import './styles/reset.css'
import './styles/globals.less'

Vue.use(VueLazyload, {
    loading: require('@/assets/loading.gif')
})

Vue.use(ElementUI);

Vue.config.productionTip = false
axios.defaults.baseURL = process.env.BASE_URL;
Vue.prototype.$axios = axios;

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
