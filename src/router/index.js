import Vue from 'vue'
import Router from 'vue-router'
import Main from '@/components/main'
import Home from '@/views/Home'
import Content from '@/components/Content'

Vue.use(Router)

const router = new Router({
	mode: 'hash',
	base: process.env.BASE_URL,
    routes: [
        {
            path: '/',
            name: 'Main',
            component: Main,
        },
        {
            path: '/home',
            name: 'Main',
            component: Home,
            children: [
                {
                    path: '/',
                    name: 'Main',
                    component: Content
                }
            ]
        }
    ]
})

router.beforeEach((to, from, next) => {
    // if (to.fullPath !== '/') {
    //     next('/')
    // } else {
    //     next()
    // }
    next()
})

export default router
