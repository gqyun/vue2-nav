export const favoriteList = state => state.favoriteList

export const usedList = state => state.usedList

export const ifEdit = state => state.ifEdit

export const menus = state => state.menus
