import * as types from './mutation-types'

const matutaions = {
    [types.SET_USED_LIST] (state, list) {
        state.usedList = list
    },
    [types.SET_FAVORITE_LIST] (state, list) {
        state.favoriteList = list
    },
    [types.SET_EDIT_STATUS](state, status) {
        state.ifEdit = status
    },
    [types.SET_MENUS](state, data) {
        state.menus = data
    },

}

export default matutaions
