import { loadUsed, loadFavorite, loadEdit, loadMenus } from '@/common/js/cache'

const state = {
    usedList: loadUsed(),
    favoriteList: loadFavorite(),
    ifEdit: loadEdit(),
    menus: loadMenus(),
}

export default state
